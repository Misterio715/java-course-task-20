package org.fmavlyutov.service;

import org.fmavlyutov.api.repository.IUserOwnedRepository;
import org.fmavlyutov.api.service.IUserOwnedService;
import org.fmavlyutov.enumerated.Sort;
import org.fmavlyutov.exception.entity.EmptyModelException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIdException;
import org.fmavlyutov.exception.field.InvalidOrEmptyIndexException;
import org.fmavlyutov.exception.user.EmptyUserIdException;
import org.fmavlyutov.model.AbstractUserOwnedModel;
import org.fmavlyutov.repository.AbstractUserOwnedRepository;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>> extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(R repository) {
        super(repository);
    }

    @Override
    public M add(final String userId, M model) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (model == null) {
            throw new EmptyModelException();
        }
        return repository.add(userId, model);
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        return repository.findAll(userId);
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        if (comparator == null) {
            return findAll(userId);
        }
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        return repository.findAll(userId, comparator);
    }

    @Override
    public List<M> findAll(final String userId, final Sort sort) {
        if (sort == null) {
            return findAll(userId);
        }
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        return repository.findAll(userId, sort.getComparator());
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        return repository.findOneById(userId, id);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (index == null || index < 0 || index >= repository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        return repository.findOneByIndex(userId, index);
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (model == null) {
            throw new EmptyModelException();
        }
        return repository.remove(userId, model);
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        return repository.removeById(userId, id);
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (index == null || index < 0 || index >= repository.getSize()) {
            throw new InvalidOrEmptyIndexException();
        }
        return repository.removeByIndex(userId, index);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        repository.clear(userId);
    }

    @Override
    public Integer getSize(final String userId) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        return repository.getSize(userId);
    }

    @Override
    public Boolean existsById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) {
            throw new EmptyUserIdException();
        }
        if (id == null || id.isEmpty()) {
            throw new InvalidOrEmptyIdException();
        }
        return repository.existsById(userId, id);
    }

}
