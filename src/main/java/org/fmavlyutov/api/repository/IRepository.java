package org.fmavlyutov.api.repository;

import org.fmavlyutov.enumerated.Sort;
import org.fmavlyutov.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    List<M> findAll(Sort sort);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

    void clear();

    Integer getSize();

    Boolean existsById(String id);

}
