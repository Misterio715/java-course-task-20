package org.fmavlyutov.api.service;

import org.fmavlyutov.enumerated.Role;
import org.fmavlyutov.model.User;

public interface IAuthService {

    User registry(String login, String password, String email);

    void login(String login, String password);

    void logout();

    boolean isAuth();

    String getUserId();

    User getUser();

    void checkRoles(Role[] roles);

}
