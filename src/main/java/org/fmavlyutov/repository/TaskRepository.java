package org.fmavlyutov.repository;

import org.fmavlyutov.api.repository.ITaskRepository;
import org.fmavlyutov.model.Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, final String id) {
        if (userId == null) {
            return Collections.emptyList();
        }
        final List<Task> bindedTasks = new ArrayList<>();
        for (final Task task : records) {
            if (id.equals(task.getProjectId())) {
                bindedTasks.add(task);
            }
        }
        return bindedTasks;
    }

}
